

console.log(fetch('https://jsonplaceholder.typicode.com/todos/'))







fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'GET'

	
})
.then(response => response.json())
.then(json => console.log(json))

fetch('https://jsonplaceholder.typicode.com/posts/', {
	method: 'GET',
	headers: {
		'Content-type': 'application/json'
	}
	
})
.then(response => response.json())
.then(function(json) {
	let arrJas = json
	let arrJas2 = arrJas.map(function(item) {
		return `${item.title}`
	})

	console.log(arrJas2)

})




async function fetchData () {
	let response =  await fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'GET'})
	let json = await response.json()
	console.log(`Status: ${response.status}, Title: ${json.title}`)


}

fetchData()



fetch('https://jsonplaceholder.typicode.com/todos/', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		'completed': false,
		'title': "Created Title",
		'userId': 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch('https://jsonplaceholder.typicode.com/todos/23', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 1,
    	"title": "Update Post",
    	"body": "Update post file"
    	

	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch('https://jsonplaceholder.typicode.com/todos/23', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		
    	"title": "Update Post",
    	"body": "Update post file",
    	"status": "Working",
    	"Date Completed": "March 19, 2020",
    	"userId": 1,

	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch('https://jsonplaceholder.typicode.com/todos/30', {
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		
    	"title": "Update Post PATCH",
    	"body": "Update post file PATCH",
    	"completed": true,
    	"Date Completed": "March 19, 2020",
    	"userId": 1,

	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch('https://jsonplaceholder.typicode.com/todos/16', {
	method: 'DELETE'})
.then(response => response.json())
.then(json => console.log(json))
