


//Fetch() method


console.log(fetch('https://jsonplaceholder.typicode.com/posts/'))




fetch('https://jsonplaceholder.typicode.com/posts/')
// then() captures the respones object and returns another promise which will eventually be resolved or rejected
.then(response => console.log(response.status))


fetch('https://jsonplaceholder.typicode.com/posts/').then(response => response.json()).then(json => console.log(json))


//async and await

async function fetchData () {
	let result = await fetch('https://jsonplaceholder.typicode.com/posts/')
	console.log(result)
	console.log(typeof result)
	console.log(result.body)


	let json = await result.json()
	console.log(json)


}


fetchData()


//Creating a post



fetch('https://jsonplaceholder.typicode.com/posts/', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 1,
    	"title": "Create Post",
    	"body": "Create post file"
	})
})
.then(response => response.json())
.then(json => console.log(json))


// Updating a post using PUT method

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		"userId": 1,
    	"title": "Update Post",
    	"body": "Update post file"
	})
})
.then(response => response.json())
.then(json => console.log(json))


// Deleting a post

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'})
.then(response => response.json())
.then(json => console.log(json))






